# Metropolis-Hastings algorithm

## Bayesian Inference scheme using MCMC

This repository contains an implementation of Metropolis-Hastings algorithm

### Posteriors plot:

<img
  src="res/posterior.png"
  alt="Alt text"
  title="Posteriors"
  style="display: inline-block; margin: 0 auto; max-width: 250px">

### Traces plot:

<img
  src="res/trace.png"
  alt="Alt text"
  title="MCMC traces"
  style="display: inline-block; margin: 0 auto; max-width: 250px">
